
const Admin = (req, res, next) => {
  if (req.user.role == 'admin') return next();
  return res.status(403).send();
};

const User = (req, res, next) => {
  if (req.user.role == 'user') return next();
  return res.status(403).send();
};

module.exports = {
  Admin,
  User
};
