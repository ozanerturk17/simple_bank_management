const jwt = require('jsonwebtoken');

module.exports = (req, res, next) => {
  // Token
  const token = req.headers.token;
  // Token not found
  if (!token) {

    res.status(401).json({ message: `Unauthorized` });
  } else {
    // Verify token
    jwt.verify(token, process.env.JWT_SECRET, (err, decoded) => {
      if (err) {
        console.log(err);
        res.status(401).json({ message: `Unauthorized` });
      } else {
        req.user = decoded;
        return next();
      }
    });
  }
};
