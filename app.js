let express = require('express');
let util = require('./util')
let path = require('path')
let mongoose = require('mongoose')
require('dotenv').config({ path: path.join(process.cwd(), '.env') });


console.log(process.env.database);
mongoose.connect(process.env.database, { useNewUrlParser: true, useUnifiedTopology: true });
if (!mongoose.connection) {
  console.log('Database connection error!');
}
// require('./toHash')

process.on('unhandledRejection', function (reason, p) {
  console.log('Possibly Unhandled Rejection at: Promise ', p, ' reason: ', reason);
  // application specific logging here
});

//We are exprecting to pass below line
util.defaults.checkDefaultAdmin()

var app = express();

// view engine setup
app.use(express.json());
app.use(express.urlencoded({ extended: false }));


app.use(require('./domain/admin/auth'))
app.use(require('./domain/user/auth'))

app.use('*', require("./middlewares/tokenValidation"));
app.use([
  ...require('./domain/admin').routers,
  ...require('./domain/user').routers,
  ...require('./domain/account').routers,
]
)


// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  console.log(err)
  res.status(err.status || 500);
  res.json(err.message);
});

module.exports = app;
