const moment = require('moment')
const Admin = require('../../domain/admin/model')
const hash = require('hash.js');

module.exports = Object.freeze({
    checkDefaultAdmin: async () => {
        let adminCount = await Admin.countDocuments()
        if (adminCount == 0) {
            const salt = process.env.salt;
            const password = hash.sha512().update("123456" + salt).digest('hex');

            let admin = new Admin({
                email: "admin@admin.com",
                name: "John",
                surname: "Doe",
                password: password,
                createdAt: moment().toDate(),
            })

            await admin.save();
        }
    }
})