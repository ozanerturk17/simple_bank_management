module.exports = Object.freeze({
    tcknValidator: (value) => {
        return new Promise((res, rej) => {
            if (!value) rej("Not found TCKN")
            value = value.toString();
            var isEleven = /^[0-9]{11}$/.test(value);
            var totalX = 0;
            for (var i = 0; i < 10; i++) {
                totalX += Number(value.substr(i, 1));
            }
            var isRuleX = totalX % 10 == value.substr(10, 1);
            var totalY1 = 0;
            var totalY2 = 0;
            for (var i = 0; i < 10; i += 2) {
                totalY1 += Number(value.substr(i, 1));
            }
            for (var i = 1; i < 10; i += 2) {
                totalY2 += Number(value.substr(i, 1));
            }
            var isRuleY = ((totalY1 * 7) - totalY2) % 10 == value.substr(9, 0);
            if (isEleven && isRuleX && isRuleY) {
                res(value)
            } else {
                rej("Not Valid TCKN")
            }
        })
    },
    currencyValidator: (value, currencies) => {
        return new Promise((res, rej) => {
            if (currencies.includes(value)) {
                return res()
            } else {
                return rej(`Not valid currency, should be in [${currencies.join(",")}`)
            }
        })
    }
})