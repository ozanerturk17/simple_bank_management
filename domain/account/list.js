const express = require('express');
const router = new express.Router();
const Account = require('./model');
const accessControlLayer = require('../../middlewares/accessControlLayer');
const { ObjectId } = require("mongoose").Types

//validators not only reject an invalid request but also tell the consumer what should it be like

router.get('/user/account', accessControlLayer.User,
  async function (req, res, next) {
    // Parse

    try {
      let pageNumber = Number(req.query.pageNumber) || 1;
      let pageSize = req.query.pageSize || 10;
      pageNumber = pageNumber < 1 ? 1 : Number(pageNumber);
      pageSize = pageSize > 50 ? 50 : Number(pageSize);
      
      let accounts = await Account.find({
        user: ObjectId(req.user.userId)
      })
        .sort({ createdAt: -1 })
        .skip(pageSize * (pageNumber - 1))
        .limit(pageSize);
      const total = await Account.countDocuments();

      return res.json({
        message: "Success",
        total: total,
        pageNumber: pageNumber,
        pageSize: pageSize,
        data: accounts.map(a => {
          return {
            _id: a._id,
            currency: a.currency,
            balance: a.balance,
            createdAt: a.createdAt,
            updatedAt: a.updatedAt,
            href: `/user/account/${a._id}`
          }
        })
      });
    } catch (err) {
      next(err);
    }
  });


module.exports = router;
