const express = require('express');
const router = new express.Router();
const { check, validationResult } = require('express-validator');
const { currencyValidator } = require('../../util/validators')
const currencies = require('../../enums/currency')
const User = require('../user/model');
const Account = require('./model');
const Transaction = require('../transaction/model');
const accessControlLayer = require('../../middlewares/accessControlLayer');
const { countDocuments } = require('./model');
const { ObjectId } = require("mongoose").Types

//validators not only reject an invalid request but also tell the consumer what should it be like
const validators = [
  check('currency').custom(v => currencyValidator(v, Object.keys(currencies))),
  check('amount').isInt({ min: 10, max: 1000 }).notEmpty().withMessage(" Amount should be defined for deposit operation (min:10, max:1000)")
];

router.put('/user/account/:sourceAccountId/transfer/:destinationAccountId', accessControlLayer.User, validators,
  async function (req, res, next) {
    // Parse
    if (!ObjectId.isValid(req.params.sourceAccountId) || ObjectId.isValid(req.params.destinationAccountId.id)) {
      return res.status(404).json({ message: 'not found' });
    }
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json(errors);
    }
    try {
      let sourceAccount = await Account.findOne({
        user: new ObjectId(req.user.userId),
        _id: new ObjectId(req.params.sourceAccountId)
      })
      if (sourceAccount.currency != req.body.currency) {
        return res.status(422).json({ message: `currency conflict expected:${sourceAccount.currency}, actual:${req.body.currency}` });
      }
      let destinationAccount = await Account.findOne({
        _id: new ObjectId(req.params.destinationAccountId)
      })
      if (destinationAccount.currency != req.body.currency) {
        return res.status(422).json({ message: `currency conflict expected:${sourceAccount.currency}, actual:${req.body.currency}` });
      }

      let transaction = new Transaction({ source: sourceAccount, type: "TRANSFER", destination: destinationAccount, amount: req.body.amount, currency: req.body.currency, state: "initial" });
      await transaction.save();

      let result = await Account.updateOne({
        _id: transaction.source, balance: { $gte: transaction.amount }
      }, {
        $inc: { balance: -transaction.amount }
      })
      console.log(result)
      if (result.n != 1) {
        await Transaction.updateOne({ _id: transaction._id }, { $set: { state: "cancel" } })
        return res.json({
          message: "Cancelled, not enough balance",
          balance:sourceAccount.balance,
          currency:sourceAccount.currency,
          href: {
            this: `/user/account/${sourceAccount._id}`,
            deposit: `/user/account/${sourceAccount._id}/deposit`
          }
        });
      } else {
        await Account.updateOne({
          _id: transaction.destination
        }, {
          $inc: { balance: transaction.amount }
        })
        sourceAccount = await Account.findOne({_id:sourceAccount._id})
        destinationUser  = await User.findOne({_id:destinationAccount.user})
        return res.json({
          message: "Success",
          href: {
            destination: destinationUser.tckn+" "+destinationUser.name +" "+destinationUser.surname,
            balance:sourceAccount.balance - req.body.amount,
            currency:sourceAccount.currency,
            this: `/user/account/${sourceAccount._id}`,
            deposit: `/user/account/${sourceAccount._id}/deposit`,
            withdraw: `/user/account/${sourceAccount._id}/withdraw`,
          }
        });
      }


    } catch (err) {
      next(err);
    }
  });


module.exports = router;
