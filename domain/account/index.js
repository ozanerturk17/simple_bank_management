module.exports = {
    routers:[
        require('./crearte'),
        require('./delete'),
        require('./get'),
        require('./deposit'),
        require('./withdraw'),
        require('./transfer'),
        require('./list'),
    ]
}