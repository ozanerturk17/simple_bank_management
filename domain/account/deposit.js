const express = require('express');
const router = new express.Router();
const { check, validationResult } = require('express-validator');
const { currencyValidator } = require('../../util/validators')
const currencies = require('../../enums/currency')
const Account = require('./model');
const Transaction = require('../transaction/model');
const accessControlLayer = require('../../middlewares/accessControlLayer');
const { ObjectId } = require("mongoose").Types

//validators not only reject an invalid request but also tell the consumer what should it be like
const validators = [
  check('currency').custom(v => currencyValidator(v, Object.keys(currencies))),
  check('amount').isInt({ min: 10, max: 1000 }).notEmpty().withMessage(" Amount should be defined for deposit operation (min:10, max:1000)")
];

router.put('/user/account/:id/deposit', accessControlLayer.User, validators,
  async function (req, res, next) {
    // Parse
    if (!ObjectId.isValid(req.params.id)) {
      return res.status(404).json({ message: 'not found' });
    }
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json(errors);
    }
    try {
      let account = await Account.findOne({
        user: new ObjectId(req.user.userId),
        _id: new ObjectId(req.params.id)
      })
      if (!account) {
        return res.status(404).json({ message: 'not found' });
      }
      if (account.currency != req.body.currency) {
        return res.status(422).json({ message: `currency conflict expected:${account.currency}, actual:${req.body.currency}` });
      }
      let transaction = new Transaction({ source: account, type: "DEPOSIT", destination: account, amount: req.body.amount, currency: req.body.currency, state: "initial" });
      await transaction.save();

      let result = await Account.update({
        user: new ObjectId(req.user.userId),
        _id: new ObjectId(req.params.id),
        currency: req.body.currency
      }, { $inc: { balance: req.body.amount } })
      if (result.n == 0 && result.nModified == 0) {

        await Transaction.updateOne({ _id: transaction._id }, { $set: { state: "cancel" } })
        return res.status(404).json({ message: 'not found' });
      } else {
        await Transaction.updateOne({ _id: transaction._id }, { $set: { state: "done" } })
      }

      return res.json({
        message: "Success",
        currency: account.currency,
        balance: account.balance + req.body.amount,
        createdAt: account.createdAt,
        updatedAt: account.updatedAt,
        href: {
          this: `/user/account/${account._id}`,
          deposit: `/user/account/${account._id}/deposit`,
          withdraw: `/user/account/${account._id}/withdraw`,
        }
      });
    } catch (err) {
      next(err);
    }
  });


module.exports = router;
