const express = require('express');
const router = new express.Router();
const moment = require('moment');
const Account = require('./model');
const User = require('../user/model');
const { check, validationResult } = require('express-validator');
const accessControlLayer = require('../../middlewares/accessControlLayer');
const { currencyValidator } = require('../../util/validators')
const currencies = require('../../enums/currency');
//validators not only reject an invalid request but also tell the consumer what should it be like
const validators = [
    check('currency').custom(v => currencyValidator(v, Object.keys(currencies))),
];


router.post('/user/account', accessControlLayer.User, validators,
    async function (req, res, next) {
        // Parse
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(400).json(errors);
        }
        try {
            let user = await User.findOne({ _id: req.user.userId })
            if (!user) {
                return res.status(404).json({ message: "user not found" });
            }

            const account = new Account({
                balance: 0,
                user: user,
                currency: req.body.currency,
                createdAt: moment().toDate(),
            });

            const result = await account.save();
            return res.json({ message: "account created", href: `/user/account/${result._id}` });
        } catch (err) {
            next(err);
        }
    });


module.exports = router;
