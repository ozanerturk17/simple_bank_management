const express = require('express');
const router = new express.Router();
const Account = require('./model');
const accessControlLayer = require('../../middlewares/accessControlLayer');
const { ObjectId } = require("mongoose").Types

//validators not only reject an invalid request but also tell the consumer what should it be like

router.get('/user/account/:id', accessControlLayer.User,
  async function (req, res, next) {
    // Parse
    if (!ObjectId.isValid(req.params.id)) {
      return res.status(404).json({ message: 'not found' });
    }
    try {

      let account = await Account.findOne({
        user: new ObjectId(req.user.userId),
        _id: new ObjectId(req.params.id)
      })
      if (!account) {
        return res.status(404).json({ message: 'not found' });
      }

      return res.json({
        message: "Success",
        currency: account.currency,
        balance: account.balance,
        createdAt: account.createdAt,
        updatedAt: account.updatedAt,
        href: {
          this: `/user/account/${account._id}`,
          deposit: `/user/account/${account._id}/deposit`
        }
      });
    } catch (err) {
      next(err);
    }
  });


module.exports = router;
