const mongoose = require('mongoose');
const currencies = require("../../enums/currency")

const accountShema = new mongoose.Schema({
    balance: {
        type: Number,
        min: 0,
    },
    user: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
    currency: {
        type: String,
        enum: Object.keys(currencies),
    },
    createdAt: Date,
    updatedAt: Date,
    pendingTransactions: [{
        type: mongoose.Schema.Types.ObjectId, ref: 'Transaction'
    }]

});


module.exports = mongoose.model('account', accountShema, 'accounts');
