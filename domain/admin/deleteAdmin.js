const express = require('express');
const router = new express.Router();
const mongoose = require("mongoose");
const Admin = require('./model');
const accessControlLayer = require('../../middlewares/accessControlLayer');

//validators not only reject an invalid request but also tell the consumer what should it be like

router.delete('/admin/:id', accessControlLayer.Admin,
  async function (req, res, next) {
    if (!mongoose.Types.ObjectId.isValid(req.params.id)) {
      return res.status(404).json({ message: 'not found' });
    }
    // Parse
    try {
      let a = await Admin.findOne({ _id: req.params.id })
      if(!a){
        return res.status(404).json({ message: 'not found' });
      }
      if(a.email == "admin@admin.com"){
        return res.status(403).json({
          message: "Unable to delete default admin",
        });
      }
      await a.remove()
      return res.json({
        message: "Delele Success",
      });
    } catch (err) {
      next(err);
    }
  });


module.exports = router;
