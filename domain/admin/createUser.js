const express = require('express');
const router = new express.Router();
const moment = require('moment');
const User = require('../user//model');
const { check, validationResult } = require('express-validator');
const accessControlLayer = require('../../middlewares/accessControlLayer');
const hash = require('hash.js');
const { tcknValidator } = require('../../util/validators')

//validators not only reject an invalid request but also tell the consumer what should it be like
const validators = [
  check('tckn').custom(tcknValidator),
  check('name').isLength({ max: 50 }).notEmpty().withMessage("name should be defined (max:50 character)"),
  check('surname').isLength({ max: 50 }).notEmpty().withMessage("surname should be defined (max:50 character)"),
  check('email').isEmail().withMessage("please eneter valid email adress"),
  check('password').isLength({ min: 6, max: 16 }).withMessage("password should be defined (min:6, max:16 character)"),
];


router.post('/user', accessControlLayer.Admin, validators,
  async function (req, res, next) {
    // Parse
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json(errors);
    }
    try {
      let existsUserEmail = await User.findOne({ email: req.body.email })
      if (existsUserEmail) {
        return res.status(409).json({ message: "user with same email already exists" });
      }
      let existsUserTckn = await User.findOne({ email: req.body.tckn })
      if (existsUserTckn) {
        return res.status(409).json({ message: "user with same tckn already exists" });
      }
      const salt = process.env.salt;
      const password = hash.sha512().update(req.body.password + salt).digest('hex');

      const user = new User({
        tckn:req.body.tckn,
        email: req.body.email,
        name: req.body.name,
        surname: req.body.surname,
        password: password,
        createdAt: moment().toDate(),
      });

      const result = await user.save();
      return res.json({ message: "user created", href: `/user/${result.tckn}` });
    } catch (err) {
      next(err);
    }
  });


module.exports = router;
