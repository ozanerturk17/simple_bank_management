const express = require('express');
const router = new express.Router();
const moment = require('moment');
const Admin = require('./model');
const { check, validationResult } = require('express-validator');
const accessControlLayer = require('../../middlewares/accessControlLayer');
const hash = require('hash.js');


//validators not only reject an invalid request but also tell the consumer what should it be like
const validators = [
  check('name').isLength({ max: 50 }).notEmpty().withMessage("name should be defined (max:50 character)"),
  check('surname').isLength({ max: 50 }).notEmpty().withMessage("surname should be defined (max:50 character)"),
  check('email').isEmail().withMessage("please eneter valid email adress"),
  check('password').isLength({ min: 6, max: 16 }).withMessage("password should be defined (min:6, max:16 character)"),
];


router.post('/admin', accessControlLayer.Admin, validators,
  async function (req, res, next) {
    // Parse
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json(errors);
    }
    try {
      let existsAdmin = await Admin.findOne({ email: req.body.email })
      if (existsAdmin) {
        return res.status(409).json({message:"admin with same email already exists"});
      }
      const salt = process.env.salt;
      const password = hash.sha512().update(req.body.password + salt).digest('hex');

      const admin = new Admin({
        email: req.body.email,
        name: req.body.name,
        surname: req.body.surname,
        password: password,
        createdAt: moment().toDate(),
      });

      const result = await admin.save();
      return res.json({ message: "admin created", href: `/admin/${result._id}` });
    } catch (err) {
      next(err);
    }
  });


module.exports = router;
