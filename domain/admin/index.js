module.exports = {
    routers:[
        require('./auth'),
        require('./createAdmin'),
        require('./getAdminList'),
        require('./getAdmin'),
        require('./updateAdmin'),
        require('./deleteAdmin'),
        require('./createUser'),
        require('./getUserList'),
    ]
}