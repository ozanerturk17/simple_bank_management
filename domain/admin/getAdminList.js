const express = require('express');
const router = new express.Router();
const Admin = require('./model');
const accessControlLayer = require('../../middlewares/accessControlLayer');


//validators not only reject an invalid request but also tell the consumer what should it be like

router.get('/admin', accessControlLayer.Admin,
  async function (req, res, next) {
    // Parse
    try {
      let pageNumber = Number(req.query.pageNumber) || 1;
      let pageSize = req.query.pageSize || 10;
      pageNumber = pageNumber < 1 ? 1 : Number(pageNumber);
      pageSize = pageSize > 50 ? 50 : Number(pageSize);
      let admins = await Admin.find()
        .sort({ createdAt: -1 })
        .skip(pageSize * (pageNumber - 1))
        .limit(pageSize);
      const total = await Admin.countDocuments();

      return res.json({
        message: "Success",
        total: total,
        pageNumber: pageNumber,
        pageSize: pageSize,
        data: admins.map(a => {
          return {
            _id:a._id,
            name: a.name,
            surname: a.surname,
            email: a.email,
            createdAt: a.createdAt,
            updatedAt: a.updatedAt,
            href: `/admin/${a._id}`
          }
        })
      });
    } catch (err) {
      next(err);
    }
  });


module.exports = router;
