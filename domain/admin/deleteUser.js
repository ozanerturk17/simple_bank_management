const express = require('express');
const router = new express.Router();
const mongoose = require("mongoose");
const User = require('../user/model');
const accessControlLayer = require('../../middlewares/accessControlLayer');

//validators not only reject an invalid request but also tell the consumer what should it be like

router.delete('/user/:tckn', accessControlLayer.User,
  async function (req, res, next) {
    if (!mongoose.Types.ObjectId.isValid(req.params.id)) {
      return res.status(404).json({ message: 'not found' });
    }
    // Parse
    try {
      let a = await User.findOne({ tcln: req.params.tckn })
      if(!a){
        return res.status(404).json({ message: 'not found' });
      }
      await a.remove()
      return res.json({
        message: "Delele Success",
      });
    } catch (err) {
      next(err);
    }
  });


module.exports = router;
