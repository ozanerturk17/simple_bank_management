const express = require('express');
const router = new express.Router();
const Admin = require('./model');
const JWT = require('jsonwebtoken');
const { check, validationResult } = require('express-validator');
const hash = require('hash.js');
const validators = [
  check('email').isEmail().withMessage("please eneter valid email adress"),
  check('password').isLength({ min: 6, max: 16 }).withMessage("password should be defined (min:6, max:16 character)"),
];

router.post('/auth/admin', validators, async function (req, res, next) {
  // Parse
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(400).json(errors);
  }
  try {
    const salt = process.env.salt;
    const hashpassword = await hash.sha512().update(req.body.password + salt).digest('hex');
    const query = {
      password: hashpassword,
      email: req.body.email,
    };
    const admin = await Admin.findOne(query);
    if (!admin) {
      return res.status(401).json({ message: "email or password wrong" });
    } else {
      const token = JWT.sign({
        userId: admin._id,
        role: "admin"
      }, process.env.JWT_SECRET, { expiresIn: '2h' });

      return res.json({
        message: `Success`,
        id: admin._id,
        name: admin.name,
        surname: admin.surname,
        token: token,
      },
      );
    }
  } catch (e) {
    next(e)
  }
});

module.exports = router;
