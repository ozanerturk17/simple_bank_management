const express = require('express');
const router = new express.Router();
const User = require('./model');
const JWT = require('jsonwebtoken');
const { check, validationResult } = require('express-validator');
const hash = require('hash.js');
const { tcknValidator } = require('../../util/validators')
const validators = [
  check('tckn').custom(tcknValidator).withMessage("please eneter valid email adress"),
  check('password').isLength({ min: 6, max: 16 }).withMessage("password should be defined (min:6, max:16 character)"),
];

router.post('/auth/user', validators, async function (req, res, next) {
  // Parse
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(400).json(errors);
  }
  try {
    console.log('here')
    const salt = process.env.salt;
    const hashpassword = await hash.sha512().update(req.body.password + salt).digest('hex');
    const query = {
      password: hashpassword,
      tckn: req.body.tckn,
    };
    const user = await User.findOne(query);
    if (!user) {
      return res.status(401).json({ message: "email or password wrong" });
    } else {
      const token = JWT.sign({
        userId: user._id,
        role: "user"
      }, process.env.JWT_SECRET, { expiresIn: '2h' });

      return res.json({
        message: `Success`,
        id: user._id,
        name: user.name,
        surname: user.surname,
        token: token,
      },
      );
    }
  } catch (e) {
    next(e)
  }
});

module.exports = router;
