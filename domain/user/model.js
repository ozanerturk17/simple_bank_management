const mongoose = require('mongoose');

//Only andmin can create users then users able to transfer their balance between their account or other users accounts 
const userSchema = new mongoose.Schema({
    tckn: String,
    email: String,
    name: String,
    surname: String,
    password: String,
    createdAt: Date,
    updatedAt: Date,
});

module.exports = mongoose.model('user', userSchema, 'users');
