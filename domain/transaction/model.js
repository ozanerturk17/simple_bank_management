const mongoose = require('mongoose');
const currencies = require("../../enums/currency")

//http://learnmongodbthehardway.com/schema/transactions/
const transactionSchema = new mongoose.Schema({
    state: {
        type: String,
        enum: ["initial", "pending", "done", "cancel"]
    },
    type: {
        type: String,
        enum: ["DEPOSIT","WITHDRAW", "TRANSFER"],
    },
    source: { type: mongoose.Schema.Types.ObjectId, ref: 'Account' },
    destination: { type: mongoose.Schema.Types.ObjectId, ref: 'Account' },
    currency: {
        type: String,
        enum: Object.keys(currencies),
    },
    amount:Number,
    createdAt: Date,
    updatedAt: Date,

});


module.exports = mongoose.model('tansaction', transactionSchema, 'tansactions');
